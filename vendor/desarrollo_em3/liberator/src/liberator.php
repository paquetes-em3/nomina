<?php
namespace desarrollo_em3\liberator;
use ReflectionClass;
use Throwable;

class liberator {
    private object $originalObject;
    private ReflectionClass $class;


    public function __construct(object $originalObject) {
        $this->originalObject = $originalObject;
        $this->class = new ReflectionClass($originalObject);
    }

    public function __get($name) {
        try {
            $property = $this->class->getProperty($name);
            $property->setAccessible(true);
            return $property->getValue($this->originalObject);
        }
        catch (Throwable $e){
            var_dump($e);
            exit;
        }

    }

    public function __set($name, $value) {
        try {
            $property = $this->class->getProperty($name);
            $property->setAccessible(true);
            $property->setValue($this->originalObject, $value);
            return $property;
        }
        catch (Throwable $e){
            var_dump($e);
            exit;
        }
    }

    public function __call($name, $args) {
        try {
            $method = $this->class->getMethod($name);
            $method->setAccessible(true);
            return $method->invokeArgs($this->originalObject, $args);
        }
        catch (Throwable $e){
            var_dump($e);
            exit;
        }
    }

}
