<?php
namespace desarrollo_em3\test;
use desarrollo_em3\error\error;
use desarrollo_em3\error\valida;
use PHPUnit\Framework\TestCase;
use stdClass;

class validaTest extends TestCase
{

    final public function test_valida_keys()
    {
        error::$en_error = false;
        $valida = new valida();
        $keys = array();
        $data = array();
        $result = $valida->valida_keys($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

        $keys=array('a');
        $data=array('a');
        $result = $valida->valida_keys($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[a] debe existir',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=array('a'=> 'zzz');
        $result = $valida->valida_keys($keys,$data);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a='xxxx';
        $result = $valida->valida_keys($keys,$data);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;


        $keys=array('a');
        $data=new stdClass();
        $data->a = new stdClass();
        $result = $valida->valida_keys($keys,$data);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;

    }

    final public function test_valida_numbers()
    {
        error::$en_error = false;
        $valida = new valida();
        $keys = array();
        $data = array();
        $result = $valida->valida_numbers($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

        $keys=array('a');
        $data=array('a');
        $result = $valida->valida_numbers($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=array('a'=> 'zzz');
        $result = $valida->valida_numbers($keys,$data);


        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a='xxxx';
        $result = $valida->valida_numbers($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = new stdClass();
        $result = $valida->valida_numbers($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = '9.9';
        $result = $valida->valida_numbers($keys,$data);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;

    }

    final public function test_valida_numbers_mayor_0()
    {
        error::$en_error = false;
        $valida = new valida();
        $keys = array();
        $data = array();
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

        $keys=array('a');
        $data=array('a');
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=array('a'=> 'zzz');
        $result = $valida->valida_numbers_mayor_0($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a='xxxx';
        $result = $valida->valida_numbers_mayor_0($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = new stdClass();
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = '-9.9';
        $result = $valida->valida_numbers_mayor_0($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = '-0.0001';
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;


        $keys=array('a');
        $data=new stdClass();
        $data->a = '-0.0000';
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero positivo mayor a 0',$result['mensaje_limpio']);

        error::$en_error = false;


        $keys=array('a');
        $data=new stdClass();
        $data->a = '0.0001';
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;

    }

    final public function test_valida_numbers_positivos()
    {
        error::$en_error = false;
        $valida = new valida();
        $keys = array();
        $data = array();
        $result = $valida->valida_numbers_positivos($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

        $keys=array('a');
        $data=array('a');
        $result = $valida->valida_numbers_positivos($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=array('a'=> 'zzz');
        $result = $valida->valida_numbers_positivos($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a='xxxx';
        $result = $valida->valida_numbers_positivos($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = new stdClass();
        $result = $valida->valida_numbers_positivos($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = '-9.9';
        $result = $valida->valida_numbers_positivos($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero positivo',$result['mensaje_limpio']);
        error::$en_error = false;



        $keys=array('a');
        $data=new stdClass();
        $data->a = '-0.0001';
        $result = $valida->valida_numbers_positivos($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero positivo',$result['mensaje_limpio']);
        error::$en_error = false;

    }

}
