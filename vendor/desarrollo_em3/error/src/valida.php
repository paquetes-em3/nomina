<?php
namespace desarrollo_em3\error;
class valida{

    private error $error;

    public function __construct()
    {
        $this->error = new error();

    }


    final public function valida_fecha(string $fecha, array $patterns): array
    {
        if(! (new valida())->valida_pattern('fecha', $patterns,$fecha)){
            return $this->error->error('Error fecha invalida, valor correcto [YYYY-MM-DD]', $fecha);
        }
        return array('mensaje'=>'fecha valida');
    }


    /**
     * FIN
     * Valida que todas las claves especificadas existan y no estén vacías en el array o el objeto proporcionado.
     *
     * @param array $keys Claves que se deben verificar en los datos.
     * @param array|object $data Datos en los cuales se verificarán las claves.
     * @return bool|array Devuelve true si todas las claves existen y no están vacías, de lo contrario, devuelve un error.
     * @example
     *  $valida = new valida();
     *  $keys = array('a');
     *  $data = array();
     *  $val = $valida->valida_keys($keys, $data);
     *  //Retorna array con mensaje de error y var errores::$en_error = true
     *
     *  errores::$en_error = false;
     *  $keys = array('a');
     *  $data = array('a'=>'value');
     *  $val = $valida->valida_keys($keys, $data);
     *  //Retorna true
     *
     *
     */
    final public function valida_keys(array $keys, $data, bool $valida_vacios = true){

        if(count($keys) === 0){
            return $this->error->error('Error $keys debe tener datos', $keys);
        }

        if(is_object($data)){
            $data = (array) $data;
        }
        foreach($keys as $key){
            if(!isset($data[$key])){
                return $this->error->error('Error $data['.$key.'] debe existir', $data);
            }
        }
        if($valida_vacios) {
            foreach ($keys as $key) {
                if (!is_string($data[$key])) {
                    continue;
                }
                if ($data[$key] === '') {
                    return $this->error->error('Error $data[' . $key . '] no puede venir vacio', $data);
                }
            }
        }
        return true;
    }

    /**
     * FIN
     * Valida que los valores de las claves especificadas en un array sean numéricos.
     *
     * Esta función toma un array de claves y un conjunto de datos. Valida que las claves
     * especificadas existan en los datos y que sus valores sean numéricos. Si ocurre un error
     * durante la validación de las claves o si algún valor no es numérico, se devuelve un array
     * con el mensaje de error correspondiente.
     *
     * @param array $keys Arreglo de claves que deben existir y ser numéricas en los datos.
     * @param array|object $data Conjunto de datos en el que se validarán las claves.
     *
     * @return bool|array Devuelve true si todas las claves existen y sus valores son numéricos.
     *                    Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    final public function valida_numbers(array $keys, $data)
    {
        $valida = $this->valida_keys($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }

        if(is_object($data)){
            $data = (array) $data;
        }

        foreach($keys as $key){
            if (!is_numeric($data[$key])) {
                return $this->error->error('Error $data[$key] '.$key.' Debe ser un numero', $data);
            }
        }
        return true;

    }

    /**
     * FIN
     * Valida que los valores de las claves especificadas en un array sean números mayores a 0.
     *
     * Esta función toma un array de claves y un conjunto de datos. Primero valida que las claves
     * especificadas existan en los datos, que sus valores sean numéricos y positivos. Luego, verifica
     * que los valores sean mayores a 0. Si ocurre un error durante alguna de las validaciones, se
     * devuelve un array con el mensaje de error correspondiente.
     *
     * @param array $keys Arreglo de claves que deben existir, ser numéricas, positivas y mayores a 0 en los datos.
     * @param array|object $data Conjunto de datos en el que se validarán las claves.
     *
     * @return bool|array Devuelve true si todas las claves existen, sus valores son numéricos, positivos y mayores a 0.
     *                    Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    final public function valida_numbers_mayor_0(array $keys, $data)
    {
        $valida = $this->valida_keys($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        $valida = $this->valida_numbers($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        $valida = $this->valida_numbers_positivos($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        if(is_object($data)){
            $data = (array) $data;
        }
        foreach($keys as $key){
            if ((float)$data[$key] <= 0.0) {
                return $this->error->error('Error $data[$key] '.$key.' Debe ser un numero positivo mayor a 0',
                    $data);
            }
        }
        return true;

    }

    /**
     * FIN
     * Valida que los valores de las claves especificadas en un array sean números positivos.
     *
     * Esta función toma un array de claves y un conjunto de datos. Primero valida que las claves
     * especificadas existan en los datos y que sus valores sean numéricos. Luego, verifica que
     * los valores sean números positivos. Si ocurre un error durante la validación de las claves,
     * si algún valor no es numérico, o si algún valor no es positivo, se devuelve un array con el
     * mensaje de error correspondiente.
     *
     * @param array $keys Arreglo de claves que deben existir, ser numéricas y positivas en los datos.
     * @param array|object $data Conjunto de datos en el que se validarán las claves.
     *
     * @return bool|array Devuelve true si todas las claves existen, sus valores son numéricos y positivos.
     *                    Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    final public function valida_numbers_positivos(array $keys, $data)
    {
        $valida = $this->valida_keys($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        $valida = $this->valida_numbers($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        if(is_object($data)){
            $data = (array) $data;
        }
        foreach($keys as $key){
            if ((float)$data[$key] < 0.0) {
                return $this->error->error('Error $data[$key] '.$key.' Debe ser un numero positivo', $data);
            }
        }
        return true;

    }

    final public function valida_pattern(string $key, array $patterns, string $txt):bool{

        $result = preg_match($patterns[$key], $txt);
        $r = false;
        if((int)$result !== 0){
            $r = true;
        }
        return $r;

    }

}