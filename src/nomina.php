<?php
namespace desarrollo_em3\nomina;

class nomina{
    final public function valida_nss($nss):array{
        $nss = trim( str_replace(array('-', ' '), '', $nss ) );
        if( ctype_digit($nss) === true && strlen($nss) === 11 ){
            return array('nss_valido'=>1);
        }
        return array('nss_valido'=>0);
    }


}