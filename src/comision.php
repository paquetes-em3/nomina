<?php
namespace desarrollo_em3\nomina;

use desarrollo_em3\error\error;
use desarrollo_em3\error\valida;
use stdClass;

class comision{
    /**
     * FIN
     * Calcula el monto restante de una comisión de contrato.
     *
     * Esta función toma un array que contiene el monto total de la comisión y el monto pagado,
     * valida las claves necesarias y calcula el monto restante. Si el monto pagado es mayor
     * que el monto total, se devuelve un error.
     *
     * @param array $contrato_comision Arreglo que contiene los datos de la comisión del contrato.
     *                                 Debe incluir las claves 'contrato_comision_monto_total' y
     *                                 'contrato_comision_monto_pagado'.
     *
     * @return float|array Devuelve el monto restante redondeado a 2 decimales si no hay errores.
     *                     Si ocurre un error durante la validación o si el monto pagado es mayor
     *                     al monto total, devuelve un array con el mensaje de error correspondiente.
     */
   private function get_monto_resto(array $contrato_comision)
    {
        $keys = array('contrato_comision_monto_total','contrato_comision_monto_pagado');
        $valida = (new valida())->valida_numbers_positivos($keys,$contrato_comision);
        if(error::$en_error){
            return (new error())->error('Error al validar contrato_comision', $valida);
        }

        $contrato_comision_monto_total = round($contrato_comision['contrato_comision_monto_total'], 2);
        $contrato_comision_monto_pagado = round($contrato_comision['contrato_comision_monto_pagado'], 2);
        $resto_compare = $contrato_comision_monto_total - $contrato_comision_monto_pagado;
        $resto_compare = round($resto_compare,2);
        if($resto_compare < 0.0){
            return (new error())->error('Error el monto pagado es mayor al total de la comision',
                $contrato_comision);
        }

        return round($resto_compare,2);
    }

    /**
     * FIN
     * Calcula el nuevo monto pagado de una comisión de contrato.
     *
     * Esta función toma un array que contiene los datos de una comisión de contrato y un monto pagado adicional.
     * Valida que la clave necesaria exista y que su valor sea numérico y positivo. Luego, redondea los valores y
     * suma el monto pagado adicional al monto pagado existente. Si alguna validación falla, se devuelve un array
     * con el mensaje de error correspondiente.
     *
     * @param array $contrato_comision Arreglo que contiene los datos de la comisión del contrato.
     *                                 Debe incluir la clave 'contrato_comision_monto_pagado'.
     * @param float $monto_pagado El monto adicional que se desea pagar.
     *
     * @return float|array Devuelve el nuevo monto pagado redondeado a 2 decimales si las validaciones son exitosas.
     *                     Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    private function contrato_comision_monto_pagado_new(array $contrato_comision, float $monto_pagado)
    {
        $keys = array('contrato_comision_monto_pagado');
        $valida = (new valida())->valida_numbers_positivos($keys,$contrato_comision);
        if(error::$en_error){
            return (new error())->error('Error al validar $contrato_comision', $valida);
        }
        $monto_pagado = round($monto_pagado,2);
        if($monto_pagado < 0.0){
            return (new error())->error('Error $monto_pagado debe ser mayor igual a 0', $monto_pagado);
        }
        $contrato_comision_monto_pagado = round($contrato_comision['contrato_comision_monto_pagado'],2);
        $contrato_comision_monto_pagado += round($monto_pagado,2);

        return round($contrato_comision_monto_pagado,2);

    }

    /**
     * FIN
     * Obtiene los datos actualizados después de un recálculo de una comisión de contrato.
     *
     * Esta función realiza varias validaciones y cálculos para obtener los datos actualizados
     * después de un recálculo de una comisión de contrato. Valida los parámetros de entrada y
     * realiza cálculos de monto pagado, monto restante y estado de la comisión. Devuelve un objeto
     * stdClass con los datos actualizados o un array con el mensaje de error correspondiente si
     * ocurre algún problema durante el proceso.
     *
     * @param array $contrato_comision Arreglo que contiene los datos de la comisión del contrato.
     * @param array $registro Arreglo que contiene los datos del registro.
     *
     * @return stdClass|array Devuelve un objeto stdClass con los datos actualizados de la comisión
     *                         si las validaciones y cálculos son exitosos. Si ocurre un error,
     *                         devuelve un array con el mensaje de error correspondiente.
     */
    private function datos_recalculo_resto(array $contrato_comision, array $registro)
    {
        $valida = $this->valida_params_recalculo($contrato_comision,$registro);
        if(error::$en_error){
            return (new error())->error('Error al validar registro', $valida);
        }
        $keys = array('contrato_comision_monto_pagado');
        $valida = (new valida())->valida_numbers_positivos($keys,$contrato_comision);
        if(error::$en_error){
            return (new error())->error('Error al validar $contrato_comision', $valida);
        }

        $params_recalculo = $this->params_recalculo($contrato_comision,$registro);
        if(error::$en_error){
            return (new error())->error('Error al obtener $params_recalculo',$params_recalculo);
        }

        $contrato_comision_monto_pagado = $this->contrato_comision_monto_pagado_new($contrato_comision,
            $params_recalculo->monto_pagado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $contrato_comision_monto_pagado',
                $contrato_comision_monto_pagado);
        }

        $monto_restos = $this->monto_restos_new($contrato_comision_monto_pagado,
            $params_recalculo->contrato_comision_monto_total);
        if(error::$en_error){
            return (new error())->error('Error al obtener $monto_restos',$monto_restos);
        }
        $status_comision_pagada = $this->status_comision_pagada_new($monto_restos);
        if(error::$en_error){
            return (new error())->error('Error al obtener $status_comision_pagada',$status_comision_pagada);
        }

        $datos = new stdClass();
        $datos->contrato_comision_monto_pagado = round($contrato_comision_monto_pagado,2);
        $datos->monto_restos = round($monto_restos,2);
        $datos->status_comision_pagada = $status_comision_pagada;

        return $datos;


    }

    /**
     * FIN
     * Calcula el monto restante de una comisión de contrato.
     *
     * Esta función toma el monto pagado y el monto total de una comisión de contrato. Valida que ambos montos
     * sean mayores o iguales a 0. Luego, calcula y redondea el monto restante. Si alguna validación falla,
     * se devuelve un array con el mensaje de error correspondiente.
     *
     * @param float $contrato_comision_monto_pagado El monto pagado de la comisión del contrato.
     * @param float $contrato_comision_monto_total El monto total de la comisión del contrato.
     *
     * @return float|array Devuelve el monto restante redondeado a 2 decimales si las validaciones son exitosas.
     *                     Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    private function monto_restos_new(
        float $contrato_comision_monto_pagado,float $contrato_comision_monto_total)
    {
        $contrato_comision_monto_total = round($contrato_comision_monto_total,2);
        $contrato_comision_monto_pagado = round($contrato_comision_monto_pagado,2);

        if($contrato_comision_monto_total < 0.0){
            return (new error())->error('Error $contrato_comision_monto_total debe ser mayor igual a 0',
                $contrato_comision_monto_total);
        }
        if($contrato_comision_monto_pagado < 0.0){
            return (new error())->error('Error $contrato_comision_monto_pagado debe ser mayor igual a 0',
                $contrato_comision_monto_pagado);
        }
        $monto_restos = $contrato_comision_monto_total - $contrato_comision_monto_pagado;
        $monto_restos = round($monto_restos,2);
        if($monto_restos < 0.0){
            return (new error())->error('Error $monto_restos debe ser mayor igual a 0',
                $monto_restos);
        }

        return round($monto_restos,2);

    }

    /**
     * FIN
     * Prepara los parámetros necesarios para el recálculo de una comisión de contrato.
     *
     * Esta función toma un array que contiene los datos de una comisión de contrato y un registro.
     * Valida que las claves necesarias existan y que sus valores sean números positivos. Luego,
     * redondea los valores y los asigna a un objeto stdClass que se devuelve.
     * Si alguna validación falla, se devuelve un array con el mensaje de error correspondiente.
     *
     * @param array $contrato_comision Arreglo que contiene los datos de la comisión del contrato.
     *                                 Debe incluir la clave 'contrato_comision_monto_total'.
     * @param array $registro Arreglo que contiene los datos del registro.
     *                        Debe incluir la clave 'monto_pagado'.
     *
     * @return stdClass|array Devuelve un objeto stdClass con los parámetros redondeados si las validaciones son exitosas.
     *                        Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    private function params_recalculo(array $contrato_comision, array $registro)
    {
        $valida = $this->valida_params_recalculo($contrato_comision,$registro);
        if(error::$en_error){
            return (new error())->error('Error al validar registro', $valida);
        }

        $monto_pagado = round($registro['monto_pagado'],2);
        $contrato_comision_monto_total = round($contrato_comision['contrato_comision_monto_total'],2);

        $data = new stdClass();

        $data->monto_pagado = $monto_pagado;
        $data->contrato_comision_monto_total = $contrato_comision_monto_total;
        return $data;

    }

    /**
     * FIN
     * Genera una fila actualizada de datos de contrato de comisión después de un recálculo.
     *
     * Esta función realiza validaciones y cálculos para generar una fila actualizada de datos de contrato
     * de comisión después de un recálculo. Valida los parámetros de entrada y el monto pagado. Luego,
     * obtiene los datos recalculados y actualiza un arreglo con los campos 'monto_resto', 'monto_pagado'
     * y 'comision_pagada'. Devuelve el arreglo actualizado si las validaciones y cálculos son exitosos.
     * Si ocurre algún error durante el proceso, devuelve un array con el mensaje de error correspondiente.
     *
     * @param array $contrato_comision Arreglo que contiene los datos de la comisión del contrato.
     * @param array $registro Arreglo que contiene los datos del registro, incluyendo 'monto_pagado'.
     *
     * @return array Devuelve un arreglo con los campos 'monto_resto', 'monto_pagado' y 'comision_pagada'
     *                     si las validaciones y cálculos son exitosos. Si ocurre un error, devuelve un array con
     *                     el mensaje de error correspondiente.
     */
    final public function row_recalculo_contrato_comision(array $contrato_comision, array $registro): array
    {
        $valida = $this->valida_params_recalculo($contrato_comision,$registro);
        if(error::$en_error){
            return (new error())->error('Error al validar registro', $valida);
        }
        $valida = $this->valida_monto_pagado($contrato_comision,$registro['monto_pagado']);
        if(error::$en_error){
            return (new error())->error('Error al validar monto pagado',$valida);
        }

        $data_recalculo = $this->datos_recalculo_resto($contrato_comision,$registro);
        if(error::$en_error){
            return (new error())->error('Error al obtener $data_recalculo',$data_recalculo);
        }

        $upd_contrato_comision = array();
        $upd_contrato_comision['monto_resto'] = round($data_recalculo->monto_restos,2);
        $upd_contrato_comision['monto_pagado'] = round($data_recalculo->contrato_comision_monto_pagado,2);
        $upd_contrato_comision['comision_pagada'] = trim($data_recalculo->status_comision_pagada);

        return $upd_contrato_comision;

    }

    /**
     * FIN
     * Determina el estado de la comisión basado en el monto restante.
     *
     * Esta función toma el monto restante de una comisión de contrato. Valida que el monto sea mayor o igual a 0.
     * Luego, determina y devuelve el estado de la comisión ('activo' o 'inactivo') basado en el monto restante.
     * Si el monto restante es menor o igual a 0, la comisión está activa; de lo contrario, está inactiva.
     * Si ocurre un error durante la validación, se devuelve un array con el mensaje de error correspondiente.
     *
     * @param float $monto_restos El monto restante de la comisión del contrato.
     *
     * @return string|array Devuelve 'activo' si la comisión está activa, 'inactivo' si está inactiva.
     *                      Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    private function status_comision_pagada_new(float $monto_restos)
    {
        $monto_restos = round($monto_restos,2);
        if($monto_restos < 0.0){
            return (new error())->error('Error $monto_restos debe ser mayor igual a 0',$monto_restos);
        }
        $status_comision_pagada = 'inactivo';
        if($monto_restos <= 0.0){
            $status_comision_pagada = 'activo';
        }
        return $status_comision_pagada;

    }

    /**
     * FIN
     * Valida el monto pagado en relación a una comisión de contrato.
     *
     * Esta función toma un array que contiene los datos de una comisión de contrato y un monto pagado.
     * Valida que las claves necesarias existan y que sus valores sean números positivos. Luego, verifica
     * que el monto pagado sea mayor a 0 y no exceda el monto restante de la comisión. Si alguna validación falla,
     * se devuelve un array con el mensaje de error correspondiente.
     *
     * @param array $contrato_comision Arreglo que contiene los datos de la comisión del contrato.
     *                                 Debe incluir las claves 'contrato_comision_monto_total' y
     *                                 'contrato_comision_monto_pagado'.
     * @param float $monto_pagado El monto pagado que se desea validar.
     *
     * @return bool|array Devuelve true si las validaciones son exitosas. Si ocurre un error,
     *                    devuelve un array con el mensaje de error correspondiente.
     */
    private function valida_monto_pagado(array $contrato_comision, float $monto_pagado)
    {
        $keys = array('contrato_comision_monto_total','contrato_comision_monto_pagado');
        $valida = (new valida())->valida_numbers_positivos($keys,$contrato_comision);
        if(error::$en_error){
            return (new error())->error('Error al validar contrato_comision', $valida);
        }
        if($monto_pagado < 0.0){
            return (new error())->error('Error $monto_pagado debe ser mayor a 0', $monto_pagado);
        }

        $resto_compare = $this->get_monto_resto($contrato_comision);
        if(error::$en_error){
            return (new error())->error('Error al obtener resto_compare',$resto_compare);
        }
        $monto_pagado = round($monto_pagado,2);

        if($monto_pagado > $resto_compare)
        {
            return (new error())->error(
                'La comisión no puede ser mayor que el monto resto.',$contrato_comision);
        }
        return true;

    }

    /**
     * FIN
     * Valida los parámetros necesarios para el recálculo de una comisión de contrato.
     *
     * Esta función valida que los parámetros proporcionados en los arreglos $registro y $contrato_comision
     * sean números positivos. Verifica las claves 'monto_pagado' en $registro y 'contrato_comision_monto_total'
     * en $contrato_comision. Devuelve true si las validaciones son exitosas. Si alguna validación falla,
     * devuelve un array con el mensaje de error correspondiente.
     *
     * @param array $contrato_comision Arreglo que contiene los datos de la comisión del contrato.
     * @param array $registro Arreglo que contiene los datos del registro.
     *
     * @return bool|array Devuelve true si las validaciones son exitosas. Si ocurre un error,
     *                    devuelve un array con el mensaje de error correspondiente.
     */
    private function valida_params_recalculo(array $contrato_comision, array $registro)
    {
        $keys = array('monto_pagado');
        $valida = (new valida())->valida_numbers_positivos($keys,$registro);
        if(error::$en_error){
            return (new error())->error('Error al validar registro', $valida);
        }
        $keys = array('contrato_comision_monto_total');
        $valida = (new valida())->valida_numbers_positivos($keys,$contrato_comision);
        if(error::$en_error){
            return (new error())->error('Error al validar registro', $valida);
        }
        return true;

    }
}