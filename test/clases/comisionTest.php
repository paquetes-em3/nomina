<?php
namespace desarrollo_em3\test\clases;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\nomina\comision;
use PHPUnit\Framework\TestCase;

class comisionTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_contrato_comision_monto_pagado_new()
    {
        error::$en_error = false;
        $obj = new comision();
        $obj = new liberator($obj);

        $contrato_comision = array();
        $contrato_comision['contrato_comision_monto_pagado'] = '0';
        $monto_pagado = 0.01;
        $result = $obj->contrato_comision_monto_pagado_new($contrato_comision,$monto_pagado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(0.01,$result);
        error::$en_error = false;



    }

    final public function test_datos_recalculo_resto()
    {
        error::$en_error = false;
        $obj = new comision();
        $obj = new liberator($obj);

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = .001;
        $contrato_comision['contrato_comision_monto_total'] = .001;
        $contrato_comision['contrato_comision_monto_pagado'] = .001;
        $result = $obj->datos_recalculo_resto($contrato_comision, $registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(0,$result->contrato_comision_monto_pagado);
        $this->assertEquals(0,$result->monto_restos);
        $this->assertEquals('activo',$result->status_comision_pagada);
        error::$en_error = false;

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = .01;
        $contrato_comision['contrato_comision_monto_total'] = .01;
        $contrato_comision['contrato_comision_monto_pagado'] = .0;
        $result = $obj->datos_recalculo_resto($contrato_comision, $registro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(.01,$result->contrato_comision_monto_pagado);
        $this->assertEquals(0,$result->monto_restos);
        $this->assertEquals('activo',$result->status_comision_pagada);

        error::$en_error = false;

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = .01;
        $contrato_comision['contrato_comision_monto_total'] = .02;
        $contrato_comision['contrato_comision_monto_pagado'] = .0;
        $result = $obj->datos_recalculo_resto($contrato_comision, $registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(.01,$result->contrato_comision_monto_pagado);
        $this->assertEquals(.01,$result->monto_restos);
        $this->assertEquals('inactivo',$result->status_comision_pagada);
        error::$en_error = false;

    }

    final public function test_get_monto_resto()
    {
        error::$en_error = false;
        $obj = new comision();
        $obj = new liberator($obj);

        $contrato_comision = array();
        $contrato_comision['contrato_comision_monto_total'] = 0;
        $contrato_comision['contrato_comision_monto_pagado'] = 0;
        $result = $obj->get_monto_resto($contrato_comision);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(0,$result);
        error::$en_error = false;

        $contrato_comision = array();
        $contrato_comision['contrato_comision_monto_total'] = 10;
        $contrato_comision['contrato_comision_monto_pagado'] = 0;
        $result = $obj->get_monto_resto($contrato_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(10,$result);

        error::$en_error = false;

        $contrato_comision = array();
        $contrato_comision['contrato_comision_monto_total'] = 10;
        $contrato_comision['contrato_comision_monto_pagado'] = 10;
        $result = $obj->get_monto_resto($contrato_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(0,$result);

        error::$en_error = false;

        $contrato_comision = array();
        $contrato_comision['contrato_comision_monto_total'] = 10;
        $contrato_comision['contrato_comision_monto_pagado'] = 10.001;
        $result = $obj->get_monto_resto($contrato_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(0,$result);

        error::$en_error = false;

        $contrato_comision = array();
        $contrato_comision['contrato_comision_monto_total'] = 10.99;
        $contrato_comision['contrato_comision_monto_pagado'] = 10.99;
        $result = $obj->get_monto_resto($contrato_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(0,$result);

        error::$en_error = false;

        $contrato_comision = array();
        $contrato_comision['contrato_comision_monto_total'] = 12.99;
        $contrato_comision['contrato_comision_monto_pagado'] = 10.99;
        $result = $obj->get_monto_resto($contrato_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(2,$result);
        error::$en_error = false;


    }

    final public function test_monto_restos_new()
    {
        error::$en_error = false;
        $obj = new comision();
        $obj = new liberator($obj);

        $contrato_comision_monto_pagado = -0.00;
        $contrato_comision_monto_total = -0.00;

        $result = $obj->monto_restos_new($contrato_comision_monto_pagado,$contrato_comision_monto_total);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(0,$result);
        error::$en_error = false;

        $contrato_comision_monto_pagado = 0.01;
        $contrato_comision_monto_total = 0.02;

        $result = $obj->monto_restos_new($contrato_comision_monto_pagado,$contrato_comision_monto_total);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(0.01,$result);
        error::$en_error = false;


    }

    final public function test_params_recalculo()
    {
        error::$en_error = false;
        $obj = new comision();
        $obj = new liberator($obj);

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = 0;
        $contrato_comision['contrato_comision_monto_total'] = 0;
        $result = $obj->params_recalculo($contrato_comision, $registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(0,$result->monto_pagado);
        $this->assertEquals(0,$result->contrato_comision_monto_total);
        error::$en_error = false;

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = 0.001;
        $contrato_comision['contrato_comision_monto_total'] = 0.009;
        $result = $obj->params_recalculo($contrato_comision, $registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(0,$result->monto_pagado);
        $this->assertEquals(0.01,$result->contrato_comision_monto_total);

        error::$en_error = false;


    }

    final public function test_row_recalculo_contrato_comision()
    {
        error::$en_error = false;
        $obj = new comision();
        //$obj = new liberator($obj);

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = .01;
        $contrato_comision['contrato_comision_monto_total'] = .01;
        $contrato_comision['contrato_comision_monto_pagado'] = .0;

        $result = $obj->row_recalculo_contrato_comision($contrato_comision,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(0,$result['monto_resto']);
        $this->assertEquals(0.01,$result['monto_pagado']);
        $this->assertEquals('activo',$result['comision_pagada']);
        error::$en_error = false;

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = .01;
        $contrato_comision['contrato_comision_monto_total'] = .02;
        $contrato_comision['contrato_comision_monto_pagado'] = .0;

        $result = $obj->row_recalculo_contrato_comision($contrato_comision,$registro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(0.01,$result['monto_resto']);
        $this->assertEquals(0.01,$result['monto_pagado']);
        $this->assertEquals('inactivo',$result['comision_pagada']);
        error::$en_error = false;


    }

    final public function test_status_comision_pagada_new()
    {
        error::$en_error = false;
        $obj = new comision();
        $obj = new liberator($obj);

        $monto_restos = -0.001;

        $result = $obj->status_comision_pagada_new($monto_restos);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('activo',$result);
        error::$en_error = false;

        $monto_restos = .01;

        $result = $obj->status_comision_pagada_new($monto_restos);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('inactivo',$result);
        error::$en_error = false;



    }

    final public function test_valida_monto_pagado()
    {
        error::$en_error = false;
        $obj = new comision();
        $obj = new liberator($obj);

        $contrato_comision = array();
        $monto_pagado = 0;
        $contrato_comision['contrato_comision_monto_total'] = 0;
        $contrato_comision['contrato_comision_monto_pagado'] = 0;
        $result = $obj->valida_monto_pagado($contrato_comision,$monto_pagado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;



    }

    final public function test_valida_params_recalculo()
    {
        error::$en_error = false;
        $obj = new comision();
        $obj = new liberator($obj);

        $contrato_comision = array();
        $registro = array();
        $result = $obj->valida_params_recalculo($contrato_comision,$registro);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar registro',$result['mensaje_limpio']);
        error::$en_error = false;

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = -0.001;
        $result = $obj->valida_params_recalculo($contrato_comision,$registro);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar registro',$result['mensaje_limpio']);

        error::$en_error = false;

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = -0.00;
        $result = $obj->valida_params_recalculo($contrato_comision,$registro);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar registro',$result['mensaje_limpio']);

        error::$en_error = false;

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = -0.00;
        $contrato_comision['contrato_comision_monto_total'] = -0.01;
        $result = $obj->valida_params_recalculo($contrato_comision,$registro);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar registro',$result['mensaje_limpio']);

        error::$en_error = false;

        $contrato_comision = array();
        $registro = array();
        $registro['monto_pagado'] = -0.00;
        $contrato_comision['contrato_comision_monto_total'] = .02;
        $result = $obj->valida_params_recalculo($contrato_comision,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;



    }


}
